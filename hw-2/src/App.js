import {Component} from "react";
import './App.scss'
import axios from "axios";
import ProductList from "./components/ProductList/ProductList";



class App extends Component {
    state = {
        modal: null,
        products: [],
        currentProduct: {},
        isOpenModal: false
    }
    openModal = (content, product) => {
        this.setState({modal: content, currentProduct: product})
    }
    closeModal = () => {
        this.setState({modal: null})
    }


    addToCart = () => {
        const {currentProduct} = this.state
        
        const cart = JSON.parse(localStorage.getItem('cart'))
        if (cart) {
            cart.push(currentProduct)
            localStorage.setItem('cart', JSON.stringify(cart))
            console.log(localStorage)
            alert('You added this item to cart!')
        } else {
            localStorage.setItem('cart', JSON.stringify([currentProduct]))

        }
        this.closeModal()
    }

    
    addToFavorite = (code) => {
        const {products} = this.state
        let favorites = []
        if (products.find(product => product.code === code)) {
            favorites = products.map(product => product.code === code ? {
                    ...product,
                    isFavorite: !product.isFavorite
                } : product
            )
            this.setState({products: favorites})
        }
        localStorage.setItem('favorites', JSON.stringify(favorites.filter(product => product.isFavorite ? product.code : '')))

    }

    componentDidMount() {
        axios('/products.json')
            .then(res => {
                const products = res.data.map(product => ({...product, isFavorite: false}));
                const favorites = JSON.parse(localStorage.getItem('favorites'))
                if (favorites) {
                    const updatedProducts = products.map(product => {
                        const favoriteProduct = favorites.find(favProduct => product.code === favProduct.code);
                        return {...product, isFavorite: favoriteProduct ? true : product.isFavorite}
                    })

                    this.setState({products: updatedProducts})
                } else {
                    this.setState({products: products})
                }
            })
    }


    render() {
        const {products, modal} = this.state

        return (
            <div className='app'>
                <ProductList products={products}
                             addToCart={this.addToCart}
                             addToFavorite={this.addToFavorite}
                             openModal={this.openModal}
                             closeModal={this.closeModal}/>
                {modal}
            </div>
        );
    }

}

export default App;




