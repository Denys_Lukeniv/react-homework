import React, {Component} from 'react';
import './Button.scss'
import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const{classes,text,handleClick}=this.props
        return (
            <button className={classes} onClick={handleClick}>
                {text}
            </button>
        );
    }
}
Button.defaultProps = {
    classes: 'btn',
    text: 'button',
    handleClick: () => { }
};

Button.propTypes = {
    classes: PropTypes.string,
    text: PropTypes.node,
    handleClick: PropTypes.func
};


export default Button;