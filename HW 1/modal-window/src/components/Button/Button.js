import React, {PureComponent } from 'react'
import styles from './Button.module.scss';

class Button extends PureComponent {
    render() {
        const {backgroundColor, children, handleClick} = this.props;

        return (
            <button className={styles.btn} style={{background:backgroundColor}} onClick={handleClick}>{children}</button>
        );
    }
}



export default Button