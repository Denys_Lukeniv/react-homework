import React, { useEffect, useState } from "react";
import './App.scss'
import axios from "axios";
import AppRoutes from "./routes/AppRoutes";
import { BrowserRouter } from "react-router-dom";
import NavMenu from "./components/NavLink/NavMenu";
import { useDispatch, useSelector } from "react-redux";
import { getProductsData, setModal, setProducts } from "./store/actionCreators/productsAC";



const App = () => {
    const modal = useSelector(state => state.prilojenie.modal)
    const products = useSelector(state => state.prilojenie.products)
    const dispatch = useDispatch()

    const openModal = (content) => {
        dispatch(setModal(content))
    }
    const closeModal = () => {
        dispatch(setModal(null))
    }


    const addToCart = (product) => {
        const cart = JSON.parse(localStorage.getItem('cart')) || []
        if (cart) {
            cart.push(product)
            localStorage.setItem('cart', JSON.stringify(cart))
        } else {
            localStorage.setItem('cart', JSON.stringify(product))
        }
        closeModal()
    }
    const deleteFromCart = (code) => {
        const cart = JSON.parse(localStorage.getItem('cart'))
        let newCart = cart.filter(productToDelete => productToDelete.code !== code)
        localStorage.setItem('cart', JSON.stringify(newCart))
        closeModal()
    }
    const addToFavorite = (code) => {
        let favorites = []
        if (products.find(product => product.code === code)) {
            favorites = products.map(product => product.code === code ? {
                ...product,
                isFavorite: !product.isFavorite
            } : product
            )
        }
        dispatch(setProducts(favorites))
        localStorage.setItem('favorites', JSON.stringify(favorites.filter(product => product.isFavorite ? product.code : '')))

    }

    useEffect(() => {
        dispatch(getProductsData())
    }, [])


    return (
        <div className='app'>
            <BrowserRouter>
                <NavMenu />
                <AppRoutes products={products}
                    addToCart={addToCart}
                    addToFavorite={addToFavorite}
                    deleteFromCart={deleteFromCart}
                    openModal={openModal}
                    closeModal={closeModal}
                />
                {modal}
            </BrowserRouter>
        </div>
    );

}

export default App;
