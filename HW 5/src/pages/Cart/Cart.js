import React from 'react';
import Product from "../../components/Product/Product";
import CartForm from '../../components/CartForm/CartForm'
import './Cart.scss'
import '../../components/ProductList/ProductList.scss'

const Cart = ({ openModal, closeModal, addToCart, addToFavorite, deleteFromCart }) => {
    const cart = JSON.parse(localStorage.getItem('cart'))
    if (cart) {
        const productCards = cart.map(product => <Product key={product.code} product={product} openModal={openModal}
            closeModal={closeModal} addToCart={addToCart}
            addToFavorite={addToFavorite}
            deleteFromCart={deleteFromCart}
            addToCartBool={false}
            deleteFromCartBool={true}
            addToFavoriteBool={false}
            isFavorite={product.isFavorite} />)

        return (
            <div className='product'>
                <div className='wrapper'>
                {productCards}
                
                </div>
                <CartForm />
            </div>
        );
    } else {
        return (
            <div className='container'>
                <h1 className='title'>No items has been added...</h1>
            </div>
        )
    }

};

export default Cart;
