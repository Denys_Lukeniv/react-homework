import { GET_PRODUCTS_DATA,SET_MODAL, SET_PRODUCTS, CART_CONFRIM } from "../actions/productsActions";
import axios from "axios";

export const getProductsData = () => async(dispatch) => {
    await axios('/items.json')
    .then(res => {
        const products = res.data.map(product => ({ ...product, isFavorite: false }));
        const favorites = JSON.parse(localStorage.getItem('favorites'))
        if (favorites) {
            const updatedProducts = products.map(product => {
                const favoriteProduct = favorites.find(favProduct => product.code === favProduct.code);
                return { ...product, isFavorite: favoriteProduct ? true : product.isFavorite }
            })
            dispatch(setProducts(updatedProducts))
        } else {
            dispatch(setProducts(products))
        }
    })

    

}
export const setModal = (content) =>({type:SET_MODAL,payload:content})

export const setProducts = (products) =>({type:SET_PRODUCTS,payload:products})

export const cartConfirm = () => ({type:CART_CONFRIM, payload: []})