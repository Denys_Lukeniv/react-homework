import React from "react";
import Cart from "../../pages/Cart/Cart";
import { SET_MODAL,SET_PRODUCTS ,CART_CONFRIM} from "../actions/productsActions";
const initialState={
    modal:null,
    products:[],
}

export default function PrilojenieReducer(state = initialState,action){
    switch (action.type){
        case SET_MODAL:
            return {...state,modal:action.payload}
        case SET_PRODUCTS:
            return {...state,products:action.payload}
        case CART_CONFRIM:
            localStorage.removeItem('cart');
            return {...state, products: action.payload}
        default:
            return state
    }
}