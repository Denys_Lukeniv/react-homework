import React from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as yup from 'yup';
import Button from '../Button/Button';
import { useDispatch, useSelector } from "react-redux";
import styles from './CartForm.module.scss'
import { cartConfirm } from '../../store/actionCreators/productsAC';

const CartForm = () => {
    const cartProducts = useSelector((state) => state.prilojenie.product)
    const dispatch = useDispatch()

    const initialValues = {
        name: '',
        surname: '',
        age: '',
        adress: '',
        phone: '',
    }

    const onSubmit = async (values) => {
        console.log('Your products:')
        console.log(cartProducts)
        console.log('Your info:')
        console.log(values);
        dispatch(cartConfirm())
    }

    const validationSchema = yup.object().shape({
        name: yup.string().required('Please, enter you name!'),
        surname: yup.string().required('Please, enter you surname'),
        age: yup.string().required('Please, enter you age'),
        adress: yup.string().required('Please, enter you adress'),
        phone: yup.string().required('Please, enter you phone'),
    })


    return (
        <div className={styles.container}>
            <h1>Confirm your order</h1>
            <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema} >
                <Form className={styles.form}>
                    <Field name='name'
                        type='text'
                        placeholder='Name'
                        className={styles.input}
                    />
                    <ErrorMessage name="name" render={error => <span className={styles.error}>{error}</span>} />
                    <Field name='surname'
                        type='text'
                        placeholder='Surname'
                        className={styles.input}
                    />
                    <ErrorMessage name="surname" render={error => <span className={styles.error}>{error}</span>} />
                    <Field name='age'
                        type='text'
                        placeholder='Age'
                        className={styles.input}
                    />
                    <ErrorMessage name="age" render={error => <span className={styles.error}>{error}</span>} />
                    <Field name='adress'
                        type='text'
                        placeholder='Adress'
                        className={styles.input}
                    />
                    <ErrorMessage name="adress" render={error => <span className={styles.error}>{error}</span>} />
                    <Field name='phone'
                        type='text'
                        placeholder='Phone'
                        className={styles.input}
                    />
                    <ErrorMessage name="phone" render={error => <span className={styles.error}>{error}</span>} />
                    <button type="submit">Confirm</button>
                </Form>
            </Formik>
        </div>
    )

}

export default CartForm