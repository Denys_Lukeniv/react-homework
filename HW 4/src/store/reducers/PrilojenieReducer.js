import React from "react";
import { SET_MODAL,SET_PRODUCTS } from "../actions/productsActions";
const initialState={
    modal:null,
    products:[],
}

export default function PrilojenieReducer(state = initialState,action){
    switch (action.type){
        case SET_MODAL:
            return {...state,modal:action.payload}
        case SET_PRODUCTS:
            return {...state,products:action.payload}
        default:
            return state
    }
}