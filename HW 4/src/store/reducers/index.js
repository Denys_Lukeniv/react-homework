import {combineReducers} from "redux";
import PrilojenieReducer from "./PrilojenieReducer";

const appReducer = combineReducers({
        prilojenie:PrilojenieReducer,
})

export default appReducer
