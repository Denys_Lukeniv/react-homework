export const GET_PRODUCTS_DATA = 'GET_PRODUCTS_DATA'
export const SET_MODAL = 'SET_MODAL'
export const SET_PRODUCTS = 'SET_PRODUCTS'
export const ADD_TO_CART = 'ADD_TO_CART'
